// Little MouseTrap-demo //
// Ila Ristola 2017 //

var app = new PIXI.Application(300, 400);
document.body.appendChild(app.view);

var mobile = false;
var sp;

window.onload = function(){
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
       mobile = true;
       sp = 10;
    } else {
       sp = 25;
    }
    }

app.renderer.view.style.position = 'absolute';app.renderer.view.style.left = '50%';app.renderer.view.style.top = '50%';app.renderer.view.style.transform = 'translate3d( -50%, -50%, 0 )';app.renderer.view.style.width = '450px';app.renderer.view.style.heigth = '600px';

// Background with two different textures
var TrapOpen = PIXI.Texture.fromImage('images/loukkuAUKI2.png');
var TrapClosed = PIXI.Texture.fromImage('images/loukkuKIINNI2.png');
var RedFlash = PIXI.Texture.fromImage('images/redScreen2.png');
var background = new PIXI.Sprite(TrapOpen);
background.anchor.set(0.5);
background.x = app.renderer.width / 2;
background.y = app.renderer.height / 2;
app.stage.addChild(background);

// Our gameobject, cheeseBlood are active if trapClosed
var cheeseClean = PIXI.Texture.fromImage('images/Cheese.png');
var cheeseBlood = PIXI.Texture.fromImage('images/CheeseBlood.png');
var cheese = new PIXI.Sprite(cheeseClean);
cheese.anchor.set(0.5);
cheese.x = 145;
cheese.y = 282;
cheese.interactive = true;
cheese.buttonMode = true;
cheese.scale.x = cheese.scale.y = 1;

// pointer events, we use pointers because it can handle mouse and touch controls
cheese
    .on('pointerdown', onDragStart)
    .on('pointerup', onDragEnd)
    .on('pointerupoutside', onDragEnd)
    .on('pointermove', onDragMove);


app.stage.addChild(cheese);

function onDragStart(event) {
this.data = event.data;
this.dragging = true;
this.cheeseX = this.data.getLocalPosition(cheese).x * cheese.scale.x;
this.cheeseY= this.data.getLocalPosition(cheese).y * cheese.scale.y;
}

function onDragEnd() {
this.dragging = false;
this.data = null;
}

function onDragMove() {
if (this.dragging) {
    var newPosition = this.data.getLocalPosition(this.parent);
    this.x = newPosition.x - this.cheeseX;
    this.y = newPosition.y - this.cheeseY;
    if (speedY < -sp || speedY > sp || speedX < -sp || speedY > sp) { // sp = mobile 10 & desktop 25 //
    activeTrap();
    this.dragging = false;

    // we put that here, cause get some nasty double function between activetrap and welldone.
    if (cheese.y > 364) {
        cheese.y = 364;
    }
}
    if (cheese.x < 60) {
        cheese.x = 60;
    }
    if (cheese.x > 235) {
        cheese.x = 235;
    }
    if (cheese.y < 262) {
        cheese.y = 262;
    }

    if (cheese.y > 365) {
        cheese.y = 365;
        cheese.interactive = false;
        wellDone();
    }
    
    
        
    }
}

// that were we handle speed detecting.
var timestamp = null;
var lastPositionX = null;
var lastPositionY = null;

document.body.addEventListener("pointermove", function(e) {
if (timestamp === null) {
    timestamp = Date.now();
    lastPositionX = e.screenX;
    lastPositionY = e.screenY;
    return;
}

var now = Date.now();
var moveT =  now - timestamp;
var moveX = e.screenX - lastPositionX;
var moveY = e.screenY - lastPositionY;
speedX = Math.round(moveX / moveT * 100);
speedY = Math.round(moveY / moveT * 100);

timestamp = now;
lastPositionX = e.screenX;
lastPositionY = e.screenY;
console.log(Math.abs(cheese.y),(cheese.x));
});


// function active after too fast dragging.
function activeTrap() {
cheese.interactive = false;
background.texture = RedFlash;
cheese.texture = cheeseBlood;
setTimeout(function() {
    background.texture = TrapClosed;
}, 50);
restartGame();
}

// Restart button with 3sek timeout before show up.
function restartGame() {
var newTry = PIXI.Sprite.fromImage('images/ReloadButton_PL.png');
newTry.anchor.set(0.5);
newTry.x = app.renderer.width / 2;
newTry.y = app.renderer.height / 2;
newTry.interactive = true;
newTry.buttonMode = true;
newTry.on('pointerdown', onClick);
setTimeout(function() {
app.stage.addChild(newTry);    
}, 1500);
}

function wellDone() {
cheese.visible = false;
var gotIt = PIXI.Sprite.fromImage('images/GameEnd.png');
gotIt.anchor.set(0.5);
gotIt.x = app.renderer.width / 2;
gotIt.y = app.renderer.height / 2;
//gotIt.scale.set(0.5);
var newGame = PIXI.Sprite.fromImage('images/PlayAgainB.png');
newGame.anchor.set(0.5);
newGame.scale.set(1);
newGame.interactive = true;
newGame.buttonMode = true;
newGame.on('pointerdown', onClick);
setTimeout(function() {
app.stage.addChild(gotIt);
gotIt.addChild(newGame);
}, 1000);
}

// Game restart
function onClick () {
    window.location.reload();
}
